const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/User');
const bcrypt = require ('bcrypt');


const validateEmail = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([ñA-Za-z\-0-9]+\.)+[ña-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

const validatePassword = (password) => {
    const re = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
    return re.test(String(password));
};


const registerStrategy = new LocalStrategy ( 
    {
        usernameField: 'email',
        passwordField: 'pass',
        passReqToCallback: true,
    }, 
    async (req, email, password, done) => {
        /**
         * 1. Comprobar que el usuario no exista previamente
         * 1.1 Comprobar que es un correo válido.
         * 1.2 Comprobar si es una contraseña válida.
         * 2. Encriptar la contraseña y guardarla en una variable
         * 3. Creamos el usuario y lo guardamos en la base de datos.
         * 4. Le iniciamos la sesión al usuario llamado a passport.authinticate();
         */
        try {

            const isValidEmail = validateEmail(email);

            if (!isValidEmail) {
                const error = new Error('Email inválido, no me hagas trampas!');
                done(error);
            };

            const isValidPassword = validatePassword(password);

            if (!isValidPassword) {
                const error = new Error('La contraseña tiene que contener de 6 a 16 de carácteres, una mayúscula y número');
                done(error);
            }


            const existingUser = await User.find({ email: email });

            if (existingUser.length) {
                const error = new Error ('The User es already registered');
                error.status = 401;
                return done(error);
            }

            const saltRounds = 10;
            const hash = await bcrypt.hash(password, saltRounds);

            const newUser = new User({
                email: email,
                pass: hash,
                name: req.body.name,
            });
            const savedUser = await newUser.save();
        
            return done(null, savedUser);
        } catch (error) {
            done(error, null)
        }
    }

    );

passport.use('registrito', registerStrategy)