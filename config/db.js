const { response } = require("express");
const mongoose = require("mongoose");
const CONFIG_DB = { useNewUrlParser: true, useUnifiedTopology: true };
const DB_URL = 'mongodb://localhost:27017/mongodb_authentication';


const connectToDb = async () =>{

    try {
        
        const response = await mongoose.connect(DB_URL, {useNewUrlParser: true,useUnifiedTopology: true,});
    
        const { host, port, name } = response.connection;
    
        console.log(`Conectado a ${name} en ${host}:${port}`);
    
    } catch(error) { 
        console.log("Error conectando a la DB", error)
    }

};

// mongoose.connect(DB_URL, {
//     useNewUrlParser: true,
//     useUnifiedTopology: true,
// })
// .then( ( response ) => {
//     const { host, port, name } = response.connection;
//     console.log( `Conectado a ${host}:${port}/${name}` );
// })
// .catch((error) => console.log('error Conectando a la DB', error));

module.exports = {
    DB_URL,
    CONFIG_DB,
    connectToDb
}