const express = require('express');
// const movies = require('./models/movies');
const passport = require('passport');
const movieRouter = require('./routes/movies.routes');
const cinemaRouter = require('./routes/cinema.routes');
const authRoutes = require('./routes/auth.routes');
require('./auth/register.strategy');

// const array = require('getData');
// const { moviesArray } = require('./seeds/movies.seed');
const { connectToDb } = require('./config/db');
connectToDb();

const PORT = 3000;

const server = express();

const router = express.Router();

// Esta línea, le dice a Express, que si recibe un (POST, PUT...) con un JSON adjunto, lo lea
// y nos lo mande al endpoint dentro de req.body;
server.use(express.json());
// Si la petición (POST, PUT.. ) y viene en formato form-urlencoded, meta la información en req.body;
server.use(express.urlencoded( { extended: false } ));

server.use(passport.initialize());

server.use('/', router);
server.use('/movies', movieRouter);
server.use('/cinema', cinemaRouter);
server.use('/auth', authRoutes);

server.use((error, req, res, next) => {
    
    const status = error.status || 500;
    const message = error.message || 'Unexpected error! There is a problem over there.';
    
    return res.status(status).json(message);

});

const serverCallBack = () => {
    console.log(`Servidor funcionando en http://localhost:${PORT}`);
}
server.listen(PORT, serverCallBack);