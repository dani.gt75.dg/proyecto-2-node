const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const cinemasSchema = new Schema(
    {
        name: { type: String, required: true },
        location: { type: String },
        movies: [ { type: mongoose.Types.ObjectId, ref: 'movies' } ]
    },
    { timestamps: true }
);

const cinemaSchema = mongoose.model('Cinema', cinemasSchema);

module.exports = cinemaSchema;
