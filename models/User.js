const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema(
    {
        // email y password en vuestro servidores
        email: { type: String, require: true },
        pass: { type: String, require: true },
        name: { type: String }
    },
    { timestamps: true }
);

const User = mongoose.model('Users', userSchema);

module.exports = User;