const express = require('express');
const passport = require('passport');
const User = require('../models/User');

const router = express.Router();

router.post('/registro', (req, res, next) =>{
    try {
        
        const done = (error, savedUser) => {
            if(error) {
                return next(error);
            }
    
            return res.status(201).json(savedUser);
        };
    
        passport.authenticate('registrito', done)(req);
        // console.log('req.body',req.body);
        // return res.status(200).json('Endpoint /registro funcionando');
    
    } catch (error) {
        console.log('Error en el registro', error);
    }

});

module.exports = router;