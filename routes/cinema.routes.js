const express = require('express');
const Cinema = require('../models/cinema');

const router = express.Router();

router.get('/', async (req, res, next) => {

    try {
        const getAllCinema = await Cinema.find().populate('movies');
        console.log(`Todo Ok, Cinemas Found ${req.url} `);
        let AllCinema = []
        getAllCinema.forEach( (element) => {
    
            AllCinema.push(element.name);
            // res.send(element.title);
            // Allmovies.push(element._id);
            //console.log(element.title);
        });
        // // console.log(getAllMovies);
        // return res.status(200).json(AllCinema);
        return res.status(200).json(getAllCinema);

    } catch (error) {
        return next(error);
    }

});


router.post('/create', async (req, res, next) => {

    try {
    /**
     * req.params ->
     * req.query ->
     * req.body ->
     */

      /**
     * Para guardar nuevos datos en la base de datos, 
     * 1. Creamos instancia del modelo
     * 2. Ejecutamos instancia.save() de la instancia
     * 
     * 3. Hacemos lo que queramos con el dato.
     */

        const newCinema = new Cinema({
            name: req.body.name,
            location: req.body.location,
            movies: req.body.movies,
        });

        const createdCinema = await newCinema.save();

        console.log('Cinema Creado correctamenta ->', createdCinema);
        
        return res.status(200).json(createdCinema); 

    } catch (error) {
        next(error);
    }

     
        
    console.log('REQ.BODY -->', req.body)
    return res.json('Respuesta aceptada');

});


router.get('/id/:id', async (req, res, next) => {
    try {
        
        const { id } = req.params;
        const cinemaId = await Cinema.findById(id);
        console.log(cinemaId);
        if ( cinemaId ) {
            return res.status(200).json(cinemaId);
        } else {
            return res.status(200).json("Cinema NOT FOUND IN DATA BASE")
        }

    } catch (error) {
        return next(error);
    }
});

router.put('/edit/:id', async (req, res, next) => {
    try {
        const { id } = req.params;
        console.log('id', id);
        console.log('body', req.body);
        
        const cinemaModify = new Cinema(req.body);
        cinemaModify._id = id;

        const cinemaUpdated = await Cinema.findByIdAndUpdate(id, cinemaModify, { new: true });

        console.log(cinemaUpdated);

        return res.status(200).json(cinemaUpdated);
    } catch(error){
        return next(error);
    }
});

router.put('/add-cinema', async (req, res, next) => {
    try {
        const { movieId, cinemaId } = req.body;

        const updatedCinema = await Cinema.findByIdAndUpdate(
            cinemaId,
            {
                // $addToSet: {movies: movieId} Hace push al array
                $addToSet: {movies: movieId} //addtoset es un método para no añadir si un elemento no existe
            },
            { new: true }
            );
        console.log(req.body);
        return res.status(200).json(updatedCinema);
    }catch(error){
        return next(error);
    }
});

router.delete('/delete/:id', async ( req, res, next ) => {

    try {

        const { id } = req.params;
        console.log(id);

        const deleted = await Cinema.findByIdAndDelete(id);
        if (deleted) {
            return res.status(200).json('Cinema deleted succesfully');
        } else {
            return res.status(404).json('No existía ese Cinema en la base de datos');
        }


    } catch (error) {
        return next(error);
    }

});


module.exports = router;