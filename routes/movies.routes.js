const express = require('express');
const movies = require('../models/movies');

const router = express.Router();

router.get('/', async (req, res, next) => {

    try {
        const getAllMovies = await movies.find();
        console.log(`Estoy, es una prueba ${req.url} `);
        let AllMovies = []
        getAllMovies.forEach( (element) => {
    
            AllMovies.push(element.title);
            // res.send(element.title);
            // Allmovies.push(element._id);
            // console.log(element.title);
        });
        // // console.log(getAllMovies);
        // return res.status(200).json(AllMovies);
        return res.status(200).json(getAllMovies);

    } catch (error) {
        return next(error);
    }

});


router.get('/genre/:genre', async (req, res, next) => {

    try {

        const genre = decodeURI(req.params.genre);
        console.log(`genre devuelve: ${req.url},${genre} `);
        console.log(`genres: ${genre}`);
        const movieGenres = await movies.find({genre});
        console.log(movieGenres);
        if (movieGenres) {

            console.log(`Movie genres: ${movieGenres}`);
            return res.status(200).json(movieGenres);

        } else {
            res.send('No se encuentra el género de movie en nuestras base de datos');
            return res.status(404).json(error);
        }


    } catch (error) {

        return next(error);

    }


});

router.get('/year/:year', async (req, res, next) => {

    try {
        
        const year = req.params.year;
        const premiere = await movies.find();
        console.log(`value ${premiere[0].year}`);
        if (premiere) {
            console.log(`Películas estrenandas apartir del año ${year}: ${premiere.title}`);
            let data = [];
            premiere.map( (i) => {
                if ( i.year >= year ) {
    
                    console.log(` i.year > year ${i.year}`);
                    data.push(i);
    
                }
            });
            return res.status(200).json(data);
        } else {
    
            console.log(`No hay películas apartir del año ${year}`);
            return res.status(404).json(error);
    
        }

    } catch (error){

        return next(error);

    };

});


/**
 * EndPoint y get que obtiene id y busca la película con respectivo id
 */

router.get('/:id', async (req, res, next) => {

    try {
        const id = req.params.id;
        console.log(`--> ${id}`);
        const id_movie = await movies.findById(id);
        if (id_movie) {

            // console.log(`Estoy es una prueba ${req.url} `);
            // console.log(`Movie: ${id_movie}`);
            return res.status(200).json(id_movie);
        } else {
            return res.status(404).json('No se encuentra el personaje que buscas');
        }
        // // console.log(getAllMovies);
    } catch (error) {
        return next(error);
    }

});


/**
 * EndPoint y get que obtiene película por un título
 */

router.get('/title/:title', async (req, res, next) => {

    try {

        const title = req.params.title;
        console.log(`--->> ${title}`);
        const movieTitle = await movies.findOne({title});

        if (movieTitle) {
            console.log(`-> ${movieTitle}`);
            // re.writeHead(200, {"Content-Type": "text/html;charset=UTF-8"});
            return res.status(200).json(movieTitle);
        } else {
            console.log("Error en title:title")
            return res.status(404).json(error);
        }
        
    } catch (error) {
        console.log("error");
        return next(error);
    }


});

router.post('/create', async (req, res, next) => {
    try {

        const newMovie = new movies({title, director, year, genre });

        const saved = await newMovie.save();
        console.log('saved', saved);
        return res.status(200).json(saved);

    } catch (error) {
        return next(error)
    }
});

router.put('/edit/:id', async (req, res, next) => {
    try {
        
        const { id } = req.params;
        console.log('id', id);
    
        const movieModify = new movies(req.body);
        movieModify._id = id;
        
        const movieUpdated = await movies.findByIdAndUpdate(id, movieModify, { new: true });
    
        return res.status(200).json(movieUpdated);

    } catch (error) {
        return next(error)
    }


});

router.delete('/delete/:id', async (req, res, next) => {
    try {
      const { id } = req.params;
  
      const deleted = await movies.findByIdAndDelete(id);
  
      if (deleted) {
        return res.status(200).json({ message: 'Character deleted succesfully!', data: deleted });
      } else {
        return res.status(404).json(`Can't find character`);
      }
    } catch (error) {
      return next(error);
    }
  });
  


module.exports= router;