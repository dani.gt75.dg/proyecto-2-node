const mongoose = require('mongoose');
const Cinema = require('../models/cinema');

const { DB_URL, CONFIG_DB } = require('../config/db');

console.log(DB_URL);
console.log(CONFIG_DB);

const cinemasArray = [
    {
        name:"Cine Center",
        location:"Santa Cruz de la Sierra",
        movies:[
            "61c0969093d314902bc79f74",
            "61c0969093d314902bc79f75",
            "61c0969093d314902bc79f78",
            "61c0969093d314902bc79f7a",
            "61c0969093d314902bc79f7b",
            "61c0969093d314902bc79f7c"
                ]
    },
    {
        name:"ABC",
        location:"Valencia",
        movies:[
            "61c0969093d314902bc79f77",
            "61c0969093d314902bc79f75",
            "61c0969093d314902bc79f79",
            "61c0969093d314902bc79f7a",
            "61c0969093d314902bc79f7b",
            "61c0969093d314902bc79f7c"

                ]
    },
    {
        name:"Cines Yelmo",
        location:"Valencia",
        movies:[
            "61c0969093d314902bc79f77",
            "61c0969093d314902bc79f75",
            "61c0969093d314902bc79f79"
                ]
    },

];

/**
 * 1. Nos conectamos a la base de datos.
 * 2. Buscaremos si ya existen personajes
 *      2.1 Si no existen, los creamos.
 *      2.2. Si existen los personajes, borramos la colección y luego añadimos el charactersArray
 * 
 * 3. Avisaremos al usuario 
 */

mongoose.connect(DB_URL, CONFIG_DB)
    .then ( async () => {
        console.log("Conectado a BBDD --> Ejecutando seed de Cinema");
        
        const allCinema = await Cinema.find();

        if (allCinema.length) {
            await Cinema.collection.drop();
            console.log("Colección de cinema eliminada con éxito");
        }
    } )
    .catch( (error) => console.log('error conectado a la DB', error) )
    .then ( async () => {
        await Cinema.insertMany(cinemasArray);
        console.log("Añadido nuevas cinema a la BBDD");
    } )
    .catch (error => console.log("Error añadiendo los nuevos personajes", error))
    .finally(() => mongoose.disconnect());