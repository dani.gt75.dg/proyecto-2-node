const mongoose = require('mongoose');
const User = require('../models/User');
const { DB_URL, CONFIG_DB } = require('../config/db');

console.log(DB_URL);
console.log(CONFIG_DB);

const usersArray = [{

    email: "ejemplo@1234.com",
    pass: "$2b$10$n4zAG49P9wWAoTC5qkYzCuvZjueL0GhOnkKJjPttKFuMgMigHvXCe",
    name: "Daniel"

  },{

    email: "ejemplo@1234.es",
    pass: "$2b$10$BaA2.kwwiQmgJvpJns8exObTwy3mYpf6uxHMBaZmB0nc8RFiACAkS",
    name: "fernando"

  },{

    email: "ejemplo@ejemplo.es",
    pass: "$2b$10$L5b3YQotyoMR5e//7EHJI.FDDcFyzOD2Tg2SF1GBAHNgvIghr5GS6",
    name: "Dani"

  },{

    email: "ejemplo@ejemplo.com",
    pass: "$2b$10$OvphE2vkHMTJYPGlTRl7AeOlpieY3l1NysvBCP8eDuRNTmwxDHVGe",
    name: "Dani"

  }];

  mongoose.connect(DB_URL, CONFIG_DB)
    .then (async () => {
        console.log("Conectado a BBDD --> Ejecutando seed de Users");
        
        const allUsers = await User.find();

        if (allUsers.length) {
            await User.collection.drop();
            console.log("Colección de Users eliminada con éxito");
        }
    })
    .catch((error) => console.log('error en la conexión de la BBDD', error))
    .then( async () => {
        await User.insertMany(usersArray);
        console.log("Añadiendo nuevos Users en la BBDD");
    } )
    .catch (error => console.log("Error añadiendo los nuevos personajes", error))
    .finally(() => mongoose.disconnect());