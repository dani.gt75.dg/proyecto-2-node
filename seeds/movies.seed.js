const mongoose = require('mongoose');
const movie = require('../models/movies');
const { DB_URL, CONFIG_DB } = require('../config/db');

console.log(DB_URL);
console.log(CONFIG_DB);

const moviesArray = [
    {
      title: 'The Matrix',
      director: 'Hermanas Wachowski',
      year: 1999,
      genre: 'Acción',
    },
    {
      title: 'The Matrix Reloaded',
      director: 'Hermanas Wachowski',
      year: 2003,
      genre: 'Acción',
    },
    {
      title: 'Buscando a Nemo',
      director: 'Andrew Stanton',
      year: 2003,
      genre: 'Animación',
    },
    {
      title: 'Buscando a Dory',
      director: 'Andrew Stanton',
      year: 2016,
      genre: 'Animación',
    },
    {
      title: 'Interestelar',
      director: 'Christopher Nolan',
      year: 2014,
      genre: 'Ciencia ficción',
    },
    {
      title: '50 primeras citas',
      director: 'Peter Segal',
      year: 2004,
      genre: 'Comedia romántica',
    },
    {
      title: 'The BabySitter',
      director: 'Carl Sagan',
      year: 2017,
      genre: 'Ficción',
    },
    {
      title: '300',
      director: 'Homer Simpson',
      year: 2007,
      genre: 'Acción',
    },
    {
      title: 'Venom 2',
      director: 'Carl Sagan',
      year: 2021,
      genre: 'Ficción',
    },
  ];

/**
 * 1. Nos conectamos a la base de datos.
 * 2. Buscaremos si ya existen personajes
 *      2.1 Si no existen, los creamos.
 *      2.2. Si existen los personajes, borramos la colección y luego añadimos el charactersArray
 * 
 * 3. Avisaremos al usuario 
 */

mongoose.connect(DB_URL, CONFIG_DB)
  .then ( async () => {
      console.log("Conectado a DB --> Ejecuando seed de Movies");

      const allMovies = await movie.find();
      

      if (allMovies.length) {
          await movie.collection.drop();
          console.log("Colección de movies eliminada con éxito");
      }
  })
  .catch((error) => console.log('error Conectando a la DB', error))
  .then ( async () => {
      await movie.insertMany(moviesArray);
      console.log("Añadido nuevas movies a la BBDD")
  })
  .catch (error => console.log("Error añadiendo los nuevos personajes", error))
  .finally(() => mongoose.disconnect());


