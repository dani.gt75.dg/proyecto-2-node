const movie = require('./models/movies');
const mongoose = require('mongoose');
const { DB_URL, CONFIG_DB } = require('./config/db');

// const arrayM = []
const getAllData = () => {

    mongoose.connect(DB_URL, CONFIG_DB)
    .then ( async () => {

        console.log("Conectado a la base de datos éxitosamente");

        const getAllMovies = await movie.find();
        getAllMovies.forEach ( (element) => {
            
            console.log(element.title);
            // // console.log(element.title);
        });
        // // console.log(array);
        // // console.log(getAllMovies);

    })
    .catch ((error) => console.log('Error al conectar a la base de datos', error))
    .finally(() => mongoose.disconnect());

}

export {getAllData};
// // const data = getAllData();
// // console.log(`--> ${data}`);
